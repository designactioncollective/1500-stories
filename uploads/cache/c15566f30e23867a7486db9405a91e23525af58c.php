<?php
    $content = get_the_content( null, false );
    $content = apply_filters( 'the_content', $content );
    $content = str_replace( ']]>', ']]&gt;', $content );
?>

{
  'ID': <?php echo e(the_ID()); ?>,
  'post_title': '<?php echo get_the_title(); ?>',
  'permalink': '<?php echo e(get_permalink()); ?>',
  'post_excerpt': '<?php echo e(get_the_excerpt()); ?>',
  'post_content': '<?php echo e(json_encode($content)); ?>',
  'post_time': '<?php echo e(get_post_time('c', true)); ?>',
  'post_date': '<?php echo e(get_the_date()); ?>',
  'post_url': '<?php echo e(get_author_posts_url(get_the_author_meta('ID'))); ?>',
  'post_author': '<?php echo e(get_the_author()); ?>',
  'acf': {
    'floor': <?php echo e(the_field('floor')); ?>

  }
},
