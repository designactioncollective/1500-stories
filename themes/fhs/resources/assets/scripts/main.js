// import external dependencies
import 'jquery';

// Import everything from autoload
import './autoload/**/*'

// import local dependencies
// import Router from './util/Router';
// import common from './routes/common';
// import home from './routes/home';
// import aboutUs from './routes/about';

// import Vue from 'vue';
// import axios from 'axios';
// import _ from 'lodash';
import fhsApp from './vue-app/vue-app-root';

// window.axios = require('axios');

/** Populate Router instance with DOM routes */
// const routes = new Router({
//   // All pages
//   common,
//   // Home page
//   home,
//   // About Us page, note the change from about-us to aboutUs.
//   aboutUs,
// });

// Load Events
// jQuery(document).ready(() => routes.loadEvents());

// fhsApp.$mount('#app'); // Linter complains unless I use the thing. Not sure what to do about that.
console.log(fhsApp);
