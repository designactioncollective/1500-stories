import Vue from 'vue';

export default Vue.component('fhs-navigation', {
  template: `
  <div class="navigation">
    <nav>
        <router-link to='/1500stories/page/1'>Page 1</router-link>
        <router-link to='/1500stories/page/2'>Page 2</router-link>
        <router-link to='/1500stories/page/3'>Page 3</router-link>
        <router-link to='/1500stories/page/4'>Page 4</router-link>
        <router-link to='/1500stories/page/5'>Page 5</router-link>
        <router-link to='/1500stories/page/6'>Page 6</router-link>
        <router-link to='/1500stories/page/7'>Page 7</router-link>
        <router-link to='/1500stories/page/8'>Page 8</router-link>
        <router-link to='/1500stories/page/9'>Page 9</router-link>
        <router-link to='/1500stories/page/10'>Page 10</router-link>
        <router-link to='/1500stories/page/11'>Page 11</router-link>
        <router-link to='/1500stories/page/12'>Page 12</router-link>
        <router-link to='/1500stories/page/13'>Page 13</router-link>
        <router-link to='/1500stories/page/14'>Page 14</router-link>
        <router-link to='/1500stories/page/15'>Page 15</router-link>
        <router-link to='/1500stories/page/16'>Page 16</router-link>
        <router-link to='/1500stories/page/17'>Page 17</router-link>
      </nav>
  </div>
  `,
});
