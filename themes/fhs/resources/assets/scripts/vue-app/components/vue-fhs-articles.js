import Vue from 'vue';
import { mapGetters, mapActions } from 'vuex';
// import { floors } from '../helpers/floors';
import _ from 'lodash';

var floors = [
  { floor: '17', classname: 'floor floor-17' },
  { floor: '16', classname: 'floor floor-16' },
  { floor: '15', classname: 'floor floor-15' },
  { floor: '14', classname: 'floor floor-14' },
  { floor: '13', classname: 'floor floor-13' },
  { floor: '12', classname: 'floor floor-12' },
  { floor: '11', classname: 'floor floor-11' },
  { floor: '10', classname: 'floor floor-10' },
  { floor: '9', classname: 'floor floor-9' },
  { floor: '8', classname: 'floor floor-8' },
  { floor: '7', classname: 'floor floor-7' },
  { floor: '6', classname: 'floor floor-6' },
  { floor: '5', classname: 'floor floor-5' },
  { floor: '4', classname: 'floor floor-4' },
  { floor: '3', classname: 'floor floor-3' },
  { floor: '2', classname: 'floor floor-2' },
  { floor: '1', classname: 'floor floor-1' },
];

export default Vue.component('fhs-articles', {
  props: ['number'],

  template: `
  <div>
    <div>On page {{ number }}</div>

    <div class="articles">
      <div class="articles__building">
        <img :src="buildingImageLink" alt="">

      </div>
          <div class="articles__list-container">

            <ul v-for='floor in floors' :class="floor.classname" v-bind:data-floor="floor.floor">
              <li v-for='article in allArticles' v-if='article.acf.floor == floor.floor' v-bind:data-floor="article.acf.floor">
                <div class="card card--hidden">

                  <article class="">
                    <header>
                      <h2 class="entry-title""><a :href="article.permalink" v-html="article.post_title"></a></h2>
                      <time class="updated" :datetime="article.date" v-html="article.date"></time>
                    </header>
                    <div class="entry-summary" v-html="article.post_content"></div>
                  </article>

                </div>
              </li>
            </ul>

          </div>

    </div>

  </div>
  `,
  // '#fhs-articles-template',

  created: function() {
    this.loadPreFetchedArticles();
  },

  mounted: function() {
    document
      .getElementById('static-stories')
      .classList.add('hide');

    // Move all lis to position stated in their acf floor/window
    this.placeStoriesByWindows();
    this.observeWhenFloorsBecomeVisible();
  },

  computed: mapGetters(['allArticles']),

  data: function() {
    return {
      buildingImageLink: `${window.location.origin}/wp-content/themes/fhs/dist/images/building.svg`,
      floors: floors,
    };
  },

  methods: {
    ...mapActions([
      'loadPreFetchedArticles',
      'loadMoreArticles',
    ]),

    observeWhenFloorsBecomeVisible: function() {
      var observer = new IntersectionObserver(
        (entries) => {
          // isIntersecting is true when element and viewport are overlapping
          // isIntersecting is false when element and viewport don't overlap
          if (entries[0].isIntersecting === true) {
            // let newPageNumber =
            //   entries[0].target.dataset.floor;
            // console.log(newPageNumber);
            // this.loadMoreArticles(newPageNumber);
          }
        },
        { threshold: [0] }
      );

      var elems = document.getElementsByClassName('floor');

      _.forEach(elems, (elem) => {
        observer.observe(elem);
      });
    },

    placeStoriesByWindows: function() {
      // Get all the story LIs
      // var storyLis = document.querySelectorAll(
      //   '.content__list-container li'
      // );
      // Place the bottom and left props
      // _.forEach(storyLis, (story) => {
      //   story.style.bottom = `${
      //     buildingMap.floors[story.dataset.floor]
      //   }`;
      //   story.style.window = `${
      //     buildingMap.windows[story.dataset.window]
      //   }`;
      // });
    },

    throttle: function(type, name, obj) {
      obj = obj || window;
      var running = false;
      var func = function() {
        if (running) {
          return;
        }
        running = true;
        requestAnimationFrame(function() {
          obj.dispatchEvent(new CustomEvent(name));
          running = false;
        });
      };
      obj.addEventListener(type, func);
    },

    scroll() {
      /* init - you can init any event */
      this.throttle('scroll', 'optimizedScroll');

      // window.addEventListener(
      //   'optimizedScroll',
      //   function() {
      //       let scrollHeight = Math.max(
      //         document.body.scrollHeight,
      //         document.documentElement.scrollHeight,
      //         document.body.offsetHeight,
      //         document.documentElement.offsetHeight,
      //         document.body.clientHeight,
      //         document.documentElement.clientHeight
      //       );
      //        bottomOfWindow =
      //         window.pageYOffset >= scrollHeight;
      // console.log(
      // 'Resource conscious scroll callback!'
      // );
      //   }
      // );

      // window.onscroll = () => {
      // let bottomOfWindow =
      //   document.documentElement.scrollTop +
      //     window.innerHeight ===
      //   document.documentElement.offsetHeight;

      // if (bottomOfWindow) {
      //   this.loadMoreArticles(2);
      //   axios
      //     .get(`https://randomuser.me/api/`)
      //     .then((response) => {
      //       this.persons.push(response.data.results[0]);
      //     });
      // }
      // };
    },
  },

  watch: {
    number(newPageNumber) {
      this.loadMoreArticles(newPageNumber);
    },
  },
});
