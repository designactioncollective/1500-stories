import axios from 'axios';
import _ from 'lodash';

const state = {
  articles: [],
};

const getters = {
  allArticles: (state) => state.articles,
};

const actions = {
  loadPreFetchedArticles({ commit }) {
    console.log(window.preloaded_data);

    commit('setArticles', window.preloaded_data);
  },

  loadMoreArticles({ commit }, newPageNumber) {
    axios
      .get(
        'http://fhs.test/wp-json/v1/1500stories/floor/' +
          newPageNumber
      )
      .then((response) => {
        commit('addArticles', response.data);
      });
  },

  // async deleteTodo({ commit }, id) {
  //   await axios.delete(`https://jsonplaceholder.typicode.com/todos/${id}`);

  //   commit('removeTodo', id);
  // },
  // async filterTodos({ commit }, e) {
  //   // Get selected number
  //   const limit = parseInt(
  //     e.target.options[e.target.options.selectedIndex].innerText
  //   );

  //   const response = await axios.get(
  //     `https://jsonplaceholder.typicode.com/todos?_limit=${limit}`
  //   );

  //   commit('setTodos', response.data);
  // },
  // async updateTodo({ commit }, updTodo) {
  //   const response = await axios.put(
  //     `https://jsonplaceholder.typicode.com/todos/${updTodo.id}`,
  //     updTodo
  //   );

  //   console.log(response.data);

  //   commit('updateTodo', response.data);
  // }
};

const mutations = {
  setArticles: (state, articles) =>
    (state.articles = articles),
  addArticles: (state, articles) => {
    let cache = state.articles;

    state.articles = _.uniqBy(
      // Dedupe the list. TODO: Should we just instead not fetch if we know the data is old?
      _.reverse(
        // Newest elements to top
        _.sortBy(
          // Sort based on id (TODO: Will need to sort by other method, if at all)
          cache.concat(articles),
          'ID' // Combine the incoming articles with the existing articles
        )
      ),
      'ID'
    );
  },

  // newTodo: (state, todo) => state.todos.unshift(todo),
  // removeTodo: (state, id) =>
  //   (state.todos = state.todos.filter(todo => todo.id !== id)),
  // updateTodo: (state, updTodo) => {
  //   const index = state.todos.findIndex(todo => todo.id === updTodo.id);
  //   if (index !== -1) {
  //     state.todos.splice(index, 1, updTodo);
  //   }
  // }
};

export default {
  state,
  getters,
  actions,
  mutations,
};
