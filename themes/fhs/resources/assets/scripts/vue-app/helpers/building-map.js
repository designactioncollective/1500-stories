var buildingMap = {
  floors: {
    1: '220px',
    2: '680px',
    3: '780px',
    4: '860px',
    5: '960px',
    6: '1040px',
    7: '1170px',
    8: '1245px',
    9: '1325px',
    10: '1390px',
    11: '1460px',
    12: '1520px',
    13: '1580px',
    14: '1640px',
    15: '1700px',
  },
  windows: {},
};

export default buildingMap;
