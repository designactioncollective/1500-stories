var floors = [
  { floor: '17', classname: 'floor floor-17' },
  { floor: '16', classname: 'floor floor-16' },
  { floor: '15', classname: 'floor floor-15' },
  { floor: '14', classname: 'floor floor-14' },
  { floor: '13', classname: 'floor floor-13' },
  { floor: '12', classname: 'floor floor-12' },
  { floor: '11', classname: 'floor floor-11' },
  { floor: '10', classname: 'floor floor-10' },
  { floor: '9', classname: 'floor floor-9' },
  { floor: '8', classname: 'floor floor-8' },
  { floor: '7', classname: 'floor floor-7' },
  { floor: '6', classname: 'floor floor-6' },
  { floor: '5', classname: 'floor floor-5' },
  { floor: '4', classname: 'floor floor-4' },
  { floor: '3', classname: 'floor floor-3' },
  { floor: '2', classname: 'floor floor-2' },
  { floor: '1', classname: 'floor floor-1' },
];

export default floors;
