import Vue from 'vue';
import router from './router/vue-app-router';
import store from './store/vue-app-store';
import fhsNavigation from './components/vue-fhs-navigation';

var fhsApp = new Vue({
  el: '#app',
  router,
  store,
  template: `
  <div>
      <fhs-navigation />
      <router-view></router-view>
    </div>
  </div>
  `,

  components: {
    fhsNavigation,
  },
});

export default { fhsApp };
