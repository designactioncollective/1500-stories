import Vue from 'vue';
import VueRouter from 'vue-router';
import fhsArticles from '../components/vue-fhs-articles';

Vue.use(VueRouter);

let appRootUrl = '/1500stories';

var routes = [
  {
    path: `${appRootUrl}/`,
    name: 'index',
    component: fhsArticles,
    props: true,
  },
  {
    path: `${appRootUrl}/page/:number`,
    name: 'fhsArticles',
    component: fhsArticles,
    props: true,
  },
  // {
  //   path: '*',
  //   component: fhs_articles,
  // },
];

export default new VueRouter({
  // Remove the hash using history mode: https://router.vuejs.org/guide/essentials/history-mode.html
  // We shouldn't need to modify the server cuz all our routes are also valid Wordpress routes.
  mode: 'history',
  routes: routes,
});
