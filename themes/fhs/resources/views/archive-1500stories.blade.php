@extends('layouts.app')

@section('content')

  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif

  <div id="app">
  Your app isn't working.
  </div>

  <!--
        WARNING: This might be a REALLY bad idea to "pre-load" data like this. I don't know what fallout may happen.
  -->
    <script type="text/javascript">
      window.preloaded_data = [
        @while (have_posts()) @php the_post() @endphp
          @include('partials.json')
        @endwhile
      ]
    </script>

  <div id="static-stories">
    @while (have_posts()) @php the_post() @endphp
      @include('partials.content-'.get_post_type())
    @endwhile
  </div>

  {!! get_the_posts_navigation() !!}
@endsection

<!-- TODO: If I'm hiding the template and if they aren't being used by default, then I'm not saving myself any time and it would be better to just keep them defined in the component definition -->
<!-- <div style="display:none;"> Hide the templates -->
  <!-- <div id="fhs-articles-template">
    <div>
      <div v-for='article in articles' v-if='article !== null'>
        <article class="post-7 post type-post status-publish format-standard hentry category-uncategorized">
          <header>
            <h2 class="entry-title"><a :href="article.link" v-html="article.title.rendered"></a></h2>
            <time class="updated" :datetime="article.date" v-html="article.date"></time>
            <fhs-byline v-bind:author="article.author" v-if="article.author.name !== null"></fhs-byline>
          </header>
          <div class="entry-summary" v-html="article.excerpt.rendered"></div>
        </article>
      </div>
    </div>
  </div> -->

  <!-- <div id="fhs-byline-template">
    <p class="byline author vcard">
      By <a :href="author.link" rel="author" class="fn" v-html="author.name">
      </a>
    </p>
  </div> -->

  <!-- <div id="fhs-root-template">
    <div>
      <nav>
        <router-link to='/page/1'>Page 1</router-link>
        <router-link to='/page/2'>Page 2</router-link>
        <router-link to='/page/3'>Page 3</router-link>
      </nav>
      <router-view></router-view>
    </div>
  </div>
</div> -->
