@php
    $content = get_the_content( null, false );
    $content = apply_filters( 'the_content', $content );
    $content = str_replace( ']]>', ']]&gt;', $content );
@endphp

{
  'ID': {{ the_ID() }},
  'post_title': '{!! get_the_title() !!}',
  'permalink': '{{ get_permalink() }}',
  'post_excerpt': '{{ get_the_excerpt() }}',
  'post_content': '{{ json_encode($content) }}',
  'post_time': '{{ get_post_time('c', true) }}',
  'post_date': '{{ get_the_date() }}',
  'post_url': '{{ get_author_posts_url(get_the_author_meta('ID')) }}',
  'post_author': '{{ get_the_author() }}',
  'acf': {
    'floor': {{ the_field('floor') }}
  }
},
