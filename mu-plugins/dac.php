<?php
/**
 * Plugin Name: Design Action plugins
 * Description: A collection of must-use plugins
 * Author: Design Action Collective
*/


/*
 * Load all mu-plugins
*/

/* Functions */
// require_once(dirname(__FILE__) . '/dac/functions/sidebar-menus.php');
// require_once(dirname(__FILE__) . '/dac/functions/menu-shortcode.php');
// require_once(dirname(__FILE__) . '/dac/functions/acf_share_options.php');
// require_once(dirname(__FILE__) . '/dac/functions/pagination.php');

/* Custom post types and taxonomies */
require_once(dirname(__FILE__) . '/dac/custom-types-tax/1500stories-cat.php');
require_once(dirname(__FILE__) . '/dac/custom-types-tax/media-type-tax.php');
require_once(dirname(__FILE__) . '/dac/custom-types-tax/social-strata-tax.php');

// require_once(dirname(__FILE__) . '/dac/custom-types-tax/updates.php');
// require_once(dirname(__FILE__) . '/dac/custom-types-tax/resource.php');
// require_once(dirname(__FILE__) . '/dac/custom-types-tax/job.php');
// //require_once(dirname(__FILE__) . '/dac/custom-types-tax/position_type.php');
// require_once(dirname(__FILE__) . '/dac/custom-types-tax/dac-staff-board.php');
// require_once(dirname(__FILE__) . '/dac/custom-types-tax/press.php');
// require_once(dirname(__FILE__) . '/dac/custom-types-tax/itn.php');
// require_once(dirname(__FILE__) . '/dac/custom-types-tax/categories.php');
// require_once(dirname(__FILE__) . '/dac/custom-types-tax/type-tax.php');
