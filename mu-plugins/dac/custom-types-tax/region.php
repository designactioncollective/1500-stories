<?php

// Register Custom Post Type
function custom_post_type_region() {

	$labels = array(
		'name'                  => _x( 'Regions', 'Post Type General Name', 'dac' ),
		'singular_name'         => _x( 'Region', 'Post Type Singular Name', 'dac' ),
		'menu_name'             => __( 'Regions', 'dac' ),
		'name_admin_bar'        => __( 'Region', 'dac' ),
		'archives'              => __( 'Region Archives', 'dac' ),
		'attributes'            => __( 'Region Attributes', 'dac' ),
		'parent_item_colon'     => __( 'Parent Region:', 'dac' ),
		'all_items'             => __( 'All Regions', 'dac' ),
		'add_new_item'          => __( 'Add New Region', 'dac' ),
		'add_new'               => __( 'Add New Region', 'dac' ),
		'new_item'              => __( 'New Region', 'dac' ),
		'edit_item'             => __( 'Edit Region', 'dac' ),
		'update_item'           => __( 'Update Region', 'dac' ),
		'view_item'             => __( 'View Region', 'dac' ),
		'view_items'            => __( 'View Regions', 'dac' ),
		'search_items'          => __( 'Search Region', 'dac' ),
		'not_found'             => __( 'Not found', 'dac' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'dac' ),
		'featured_image'        => __( 'Featured Image', 'dac' ),
		'set_featured_image'    => __( 'Set featured image', 'dac' ),
		'remove_featured_image' => __( 'Remove featured image', 'dac' ),
		'use_featured_image'    => __( 'Use as featured image', 'dac' ),
		'insert_into_item'      => __( 'Insert into region', 'dac' ),
		'uploaded_to_this_item' => __( 'Uploaded to this region', 'dac' ),
		'items_list'            => __( 'Region list', 'dac' ),
		'items_list_navigation' => __( 'Region list navigation', 'dac' ),
		'filter_items_list'     => __( 'Filter Region list', 'dac' ),
	);
	$args = array(
		'label'                 => __( 'Region', 'dac' ),
		'description'           => __( 'Post Type Description', 'dac' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'excerpt' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-location-alt',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'show_in_rest'          => true,
	);
	register_post_type( 'region', $args );

}
add_action( 'init', 'custom_post_type_region', 0 );
