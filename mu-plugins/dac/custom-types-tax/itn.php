<?php
/*
Plugin Name: DAC - Staff and Attorneys
Description: <strong>Staff and Attorneys</strong> Functionality
Version:     0.0.1
Author:      Design Action
Author URI:  http://designaction.org
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

// Register Custom Post Type
function custom_post_type_itn() {

	$labels = array(
		'name'                  => _x( 'In the News', 'Post Type General Name', 'dac' ),
		'singular_name'         => _x( 'In the News', 'Post Type Singular Name', 'dac' ),
		'menu_name'             => __( 'In the News', 'dac' ),
		'name_admin_bar'        => __( 'In the News', 'dac' ),
		'archives'              => __( 'In the News Archives', 'dac' ),
		'attributes'            => __( 'Item Attributes', 'dac' ),
		'parent_item_colon'     => __( 'Parent Item:', 'dac' ),
		'all_items'             => __( 'All In the News', 'dac' ),
		'add_new_item'          => __( 'Add New In the News', 'dac' ),
		'add_new'               => __( 'Add New', 'dac' ),
		'new_item'              => __( 'New Item', 'dac' ),
		'edit_item'             => __( 'Edit Item', 'dac' ),
		'update_item'           => __( 'Update Item', 'dac' ),
		'view_item'             => __( 'View Item', 'dac' ),
		'view_items'            => __( 'View Items', 'dac' ),
		'search_items'          => __( 'Search In the News', 'dac' ),
		'not_found'             => __( 'Not found', 'dac' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'dac' ),
		'featured_image'        => __( 'Featured Image', 'dac' ),
		'set_featured_image'    => __( 'Set featured image', 'dac' ),
		'remove_featured_image' => __( 'Remove featured image', 'dac' ),
		'use_featured_image'    => __( 'Use as featured image', 'dac' ),
		'insert_into_item'      => __( 'Insert into item', 'dac' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'dac' ),
		'items_list'            => __( 'In the News list', 'dac' ),
		'items_list_navigation' => __( 'In the News list navigation', 'dac' ),
		'filter_items_list'     => __( 'Filter In the News list', 'dac' ),
	);
	$rewrite = array(
		'slug'                  => 'in-the-news',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'In the News', 'dac' ),
		'description'           => __( 'Post Type Description', 'dac' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields' ),
		'taxonomies'            => array( 'catalog'),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-clipboard',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'page',
		'show_in_rest'          => true,
	);
	register_post_type( 'itn', $args );

}
add_action( 'init', 'custom_post_type_itn', 0 );
