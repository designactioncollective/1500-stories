<?php

/*
Plugin Name: DAC - Updates
Description: <strong>Updates</strong> Functionality
Version:     0.0.1
Author:      Design Action
Author URI:  http://designaction.org
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

// Register Custom Post Type
function custom_post_type_updates() {

	$labels = array(
		'name'                  => _x( 'Updates', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Update', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Updates', 'text_domain' ),
		'name_admin_bar'        => __( 'Update', 'text_domain' ),
		'archives'              => __( 'Updates', 'text_domain' ),
		'attributes'            => __( 'Update Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Update:', 'text_domain' ),
		'all_items'             => __( 'All Updates', 'text_domain' ),
		'add_new_item'          => __( 'Add New Update', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Update', 'text_domain' ),
		'edit_item'             => __( 'Edit Update', 'text_domain' ),
		'update_item'           => __( 'Update Update', 'text_domain' ),
		'view_item'             => __( 'View Update', 'text_domain' ),
		'view_items'            => __( 'View Updates', 'text_domain' ),
		'search_items'          => __( 'Search Update', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into Update', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Updates list', 'text_domain' ),
		'items_list_navigation' => __( 'Update list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter Updates list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Update', 'text_domain' ),
		'description'           => __( 'Post Type Description', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields', 'excerpt' ),
		'taxonomies'            => array( 'catalog'),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'show_in_rest'          => true,
	);
	register_post_type( 'update', $args );

}
add_action( 'init', 'custom_post_type_updates', 0 );
?>
