<?php // Register Custom Taxonomy

function type_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Media Types', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Media Type', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Media Type', 'text_domain' ),
		'all_items'                  => __( 'All Media Types', 'text_domain' ),
		'parent_item'                => __( 'Parent Type', 'text_domain' ),
		'parent_item_colon'          => __( 'Parent Type:', 'text_domain' ),
		'new_item_name'              => __( 'New Type Name', 'text_domain' ),
		'add_new_item'               => __( 'Add New Type', 'text_domain' ),
		'edit_item'                  => __( 'Edit Type', 'text_domain' ),
		'update_item'                => __( 'Update Type', 'text_domain' ),
		'view_item'                  => __( 'View Type', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separate types with commas', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or remove types', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
		'popular_items'              => __( 'Popular Media Types', 'text_domain' ),
		'search_items'               => __( 'Search types', 'text_domain' ),
		'not_found'                  => __( 'Not Found', 'text_domain' ),
		'no_terms'                   => __( 'No types', 'text_domain' ),
		'items_list'                 => __( 'Media Types list', 'text_domain' ),
		'items_list_navigation'      => __( 'Media Types list navigation', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'show_in_rest'               => true,
		'rewrite'                    => array( 'slug' => 'media_type' ),
	);
	register_taxonomy( 'media_type', array( '1500stories' ), $args );

}
add_action( 'init', 'type_taxonomy', 0 );

?>
