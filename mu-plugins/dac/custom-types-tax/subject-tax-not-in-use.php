<?php // Register Custom Taxonomy
function custom_taxonomy_subj() {


//Topics
	$labels = array(
		'name'                       => _x( 'Subjects', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Subject', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Subject', 'text_domain' ),
		'all_items'                  => __( 'All Subjects', 'text_domain' ),
		'parent_item'                => __( 'Parent Subject', 'text_domain' ),
		'parent_item_colon'          => __( 'Parent Subject:', 'text_domain' ),
		'new_item_name'              => __( 'New Subject Name', 'text_domain' ),
		'add_new_item'               => __( 'Add New Subject', 'text_domain' ),
		'edit_item'                  => __( 'Edit Subject', 'text_domain' ),
		'update_item'                => __( 'Update Subject', 'text_domain' ),
		'view_item'                  => __( 'View Subject', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separate subject with commas', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or remove subjects', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
		'popular_items'              => __( 'Popular Subjects', 'text_domain' ),
		'search_items'               => __( 'Search Subjects', 'text_domain' ),
		'not_found'                  => __( 'Not Found', 'text_domain' ),
		'no_terms'                   => __( 'No items', 'text_domain' ),
		'items_list'                 => __( 'Items list', 'text_domain' ),
		'items_list_navigation'      => __( 'Items list navigation', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'show_in_rest'							 => true,
	);
	register_taxonomy( 'subject', array( 'resource' ), $args );

}
add_action( 'init', 'custom_taxonomy_subj', 0 );

?>
