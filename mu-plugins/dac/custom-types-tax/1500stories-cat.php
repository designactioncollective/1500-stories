<?php

/*
Plugin Name: DAC - Updates
Description: <strong>Updates</strong> Functionality
Version:     0.0.1
Author:      Design Action
Author URI:  http://designaction.org
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

// Register Custom Post Type
function fifteen_hundred_stories() {

	$labels = array(
		'name'                  => _x( '1500 Stories', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( '1500 Story', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( '1500 Stories', 'text_domain' ),
		'name_admin_bar'        => __( '1500 Stories', 'text_domain' ),
		'archives'              => __( '1500 Story Archives', 'text_domain' ),
		'attributes'            => __( 'Story Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Stories', 'text_domain' ),
		'add_new_item'          => __( 'Add New Story', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Story', 'text_domain' ),
		'edit_item'             => __( 'Edit Story', 'text_domain' ),
		'update_item'           => __( 'Update Story', 'text_domain' ),
		'view_item'             => __( 'View Story', 'text_domain' ),
		'view_items'            => __( 'View Stories', 'text_domain' ),
		'search_items'          => __( 'Search Stories', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( '1500 Story Type', 'text_domain' ),
		'description'           => __( '1500 Story Type Description', 'text_domain' ),
		'labels'                => $labels,
		'taxonomies'            => array( 'media-type', 'social-strata' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
		'support'               => array(
			'title',
			'editor',
			'excerpt',
			'thumbnail',
			'revisions',
			'custom-fields',
		),
		// 'rewrite'            => array( 'slug' => '1500story' ),
    	'rest_base'          => '1500stories',
	);
	register_post_type( '1500stories', $args );

}
add_action( 'init', 'fifteen_hundred_stories', 0 );

?>
