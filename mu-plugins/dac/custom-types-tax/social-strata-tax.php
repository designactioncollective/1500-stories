<?php // Register Custom Taxonomy

// Register Custom Taxonomy
function social_strata_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Social Stratum', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Social Stratum', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Social Strata', 'text_domain' ),
		'all_items'                  => __( 'All Social Strata', 'text_domain' ),
		'parent_item'                => __( 'Parent Social Strata', 'text_domain' ),
		'parent_item_colon'          => __( 'Parent Social Strata:', 'text_domain' ),
		'new_item_name'              => __( 'New Social Stratum Name', 'text_domain' ),
		'add_new_item'               => __( 'Add New Social Stratum', 'text_domain' ),
		'edit_item'                  => __( 'Edit Social Stratum', 'text_domain' ),
		'update_item'                => __( 'Update Social Stratum', 'text_domain' ),
		'view_item'                  => __( 'View Social Stratum', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separate social strata with commas', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or remove social strata', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
		'popular_items'              => __( 'Popular Social Strata', 'text_domain' ),
		'search_items'               => __( 'Search Social Strata', 'text_domain' ),
		'not_found'                  => __( 'Not Found', 'text_domain' ),
		'no_terms'                   => __( 'No social strata', 'text_domain' ),
		'items_list'                 => __( 'Social strata list', 'text_domain' ),
		'items_list_navigation'      => __( 'Items list navigation', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'show_in_rest'               => true,
		'rest_base'                  => 'social-strata',
	);
	register_taxonomy( 'social-strata', array( '1500stories' ), $args );

}
add_action( 'init', 'social_strata_taxonomy', 0 );

?>
