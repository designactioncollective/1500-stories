<?php
/*
Plugin Name: DAC - Staff and Attorneys
Description: <strong>Staff and Attorneys</strong> Functionality
Version:     0.0.1
Author:      Design Action
Author URI:  http://designaction.org
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );


// Register Custom Taxonomy
function custom_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Position Types', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Position Type', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Position Type', 'text_domain' ),
		'all_items'                  => __( 'All Position Types', 'text_domain' ),
		'parent_item'                => __( 'Parent Position Type', 'text_domain' ),
		'parent_item_colon'          => __( 'Parent Position Type:', 'text_domain' ),
		'new_item_name'              => __( 'New Position Type Name', 'text_domain' ),
		'add_new_item'               => __( 'Add New Position Type', 'text_domain' ),
		'edit_item'                  => __( 'Edit Position Type', 'text_domain' ),
		'update_item'                => __( 'Update Position Type', 'text_domain' ),
		'view_item'                  => __( 'View Position Type', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
		'popular_items'              => __( 'Popular Items', 'text_domain' ),
		'search_items'               => __( 'Search Items', 'text_domain' ),
		'not_found'                  => __( 'Not Found', 'text_domain' ),
		'no_terms'                   => __( 'No items', 'text_domain' ),
		'items_list'                 => __( 'Items list', 'text_domain' ),
		'items_list_navigation'      => __( 'Items list navigation', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'show_in_rest'					=> true

	);
	register_taxonomy( 'position_type', array( 'position' ), $args );

}
add_action( 'init', 'custom_taxonomy', 0 );
