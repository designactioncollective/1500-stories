<?php
/*
Plugin Name: DAC - Staff and Attorneys
Description: <strong>Staff and Attorneys</strong> Functionality
Version:     0.0.1
Author:      Design Action
Author URI:  http://designaction.org
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

// Register Staff & Attorney Custom Post Types
function dac_custom_post_type_staff() {

	// Staff and Attorney
	$labels = array(
		'name'                  => _x( 'Staff and Board', 'Post Type General Name', 'dac' ),
		'singular_name'         => _x( 'Person', 'Post Type Singular Name', 'dac' ),
		'menu_name'             => __( 'People', 'dac' ),
		'name_admin_bar'        => __( 'People', 'dac' ),
		'archives'              => __( 'People Archives', 'dac' ),
		'parent_item_colon'     => __( 'Parent Person:', 'dac' ),
		'all_items'             => __( 'All People', 'dac' ),
		'add_new_item'          => __( 'Add New Person', 'dac' ),
		'add_new'               => __( 'Add New', 'dac' ),
		'new_item'              => __( 'New Person', 'dac' ),
		'edit_item'             => __( 'Edit Person', 'dac' ),
		'update_item'           => __( 'Update Person', 'dac' ),
		'view_item'             => __( 'View Person', 'dac' ),
		'search_items'          => __( 'Search People', 'dac' ),
		'not_found'             => __( 'Not found', 'dac' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'dac' ),
		'featured_image'        => __( 'Featured Image', 'dac' ),
		'set_featured_image'    => __( 'Set featured image', 'dac' ),
		'remove_featured_image' => __( 'Remove featured image', 'dac' ),
		'use_featured_image'    => __( 'Use as featured image', 'dac' ),
		'insert_into_item'      => __( 'Insert into Person', 'dac' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Person', 'dac' ),
		'items_list'            => __( 'People list', 'dac' ),
		'items_list_navigation' => __( 'People list navigation', 'dac' ),
		'filter_items_list'     => __( 'Filter People list', 'dac' ),
	);
	$args = array(
		'label'                 => __( 'People', 'dac' ),
		'description'           => __( 'People', 'dac' ),
		'labels'                => $labels,
		'supports'              => array( 'revisions', 'page-attributes', 'custom-fields', 'thumbnail', 'editor' ),
		'taxonomies'            => array('role'),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-id-alt',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'people', $args );

}
add_action( 'init', 'dac_custom_post_type_staff', 0 );


// Auto add and update Title field for Staff and Attorney
// via https://support.advancedcustomfields.com/forums/topic/replacing-custom-post-type-post-title-with-an-acf/
function person_update_title( $value, $post_id, $field ) {

	$new_title = get_field( 'first_name', $post_id) . ' ' . $value;
	$new_slug = sanitize_title( $new_title );

	// update post
	$person_postdata = array(
		'ID'          => $post_id,
		'post_title'  => $new_title,
		'post_name'   => $new_slug,
	);

	if ( ! wp_is_post_revision( $post_id ) ){

		// unhook this function so it doesn't loop infinitely
		remove_action('save_post', 'person_update_title');

		// update the post, which calls save_post again
		wp_update_post( $person_postdata );

		// re-hook this function
		add_action('save_post', 'person_update_title');
	}

	return $value;
}

add_filter('acf/update_value/name=last_name', 'person_update_title', 10, 3);




// Register Custom Role Taxonomy
function dac_person_taxonomy() {

    $labels = array(
        'name'                       => _x( 'Roles', 'Taxonomy General Name', 'dac' ),
        'singular_name'              => _x( 'Role', 'Taxonomy Singular Name', 'dac' ),
        'menu_name'                  => __( 'Role', 'dac' ),
        'all_items'                  => __( 'All Roles', 'dac' ),
        'parent_item'                => __( 'Parent Role', 'dac' ),
        'parent_item_colon'          => __( 'Parent Role:', 'dac' ),
        'new_item_name'              => __( 'New Role Name', 'dac' ),
        'add_new_item'               => __( 'Add New Role', 'dac' ),
        'edit_item'                  => __( 'Edit Role', 'dac' ),
        'update_item'                => __( 'Update Role', 'dac' ),
        'view_item'                  => __( 'View Role', 'dac' ),
        'separate_items_with_commas' => __( 'Separate roles with commas', 'dac' ),
        'add_or_remove_items'        => __( 'Add or remove roles', 'dac' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'dac' ),
        'popular_items'              => __( 'Popular Roles', 'dac' ),
        'search_items'               => __( 'Search Roles', 'dac' ),
        'not_found'                  => __( 'Not Found', 'dac' ),
        'no_terms'                   => __( 'No roles', 'dac' ),
        'items_list'                 => __( 'Roles list', 'dac' ),
        'items_list_navigation'      => __( 'Roles list navigation', 'dac' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => false,
    );
    register_taxonomy( 'role', array( 'people' ), $args );

}
add_action( 'init', 'dac_person_taxonomy', 0 );
